#!/usr/bin/env bash
# set -Eeuo pipefail

# !!! BEFORE RUNNING THIS SCRIPT BACKUP YOUR MINIFLUX DATABASE !!!
# If using postgresql, switch to user postgres and run this:
# pg_dump miniflux2 > miniflux_backup.sql

tag=$(curl -s https://api.github.com/repos/miniflux/v2/releases/latest | grep "tag_name")

release=$(echo $tag | cut -c14-19)
version="$(miniflux --version)"

if test "$release" = "$version"; then

	echo "Miniflux: No update available"

else

	# Disconnect all users
	miniflux -flush-sessions -c=/etc/miniflux.conf

	# Stop miniflux service
	systemctl stop miniflux

	# Installation platform
	platform="/miniflux-linux-amd64"

	# Upgrade miniflux binary
	download=$(curl -s https://api.github.com/repos/miniflux/v2/releases/latest \
	| grep "tag_name" \
	| awk -v p="$platform" '{print "https://github.com/miniflux/v2/releases/download/" substr($2, 2, length($2)-3) p}') \
	; curl -L -o /usr/bin/miniflux $download

	# Set the binary as executable
	chmod +x /usr/bin/miniflux

	# Run database migrations
	miniflux -migrate -c=/etc/miniflux.conf

	# Start Miniflux service
	systemctl start miniflux

fi
